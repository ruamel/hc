Changelog
=========

.. include:: includes/all.rst

This project adheres to `Semantic Versioning <http://semver.org/spec/v2.0.0.html>`__
and `human-readable changelog <http://keepachangelog.com/en/0.3.0/>`__.


`hc master`_ - unreleased
-------------------------

.. _hc master: https://gitlab.com/ypid/hc/compare/v0.1.0...master


hc v0.1.0 - 2017-03-02
----------------------

Added
~~~~~

- Initial coding and design. [ypid_]
